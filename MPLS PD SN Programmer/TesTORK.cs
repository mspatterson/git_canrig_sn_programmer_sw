﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Canrig_SN_Programmer
{
    class TesTORK
    {
        CanrigStandardCRC canrigCRC = new CanrigStandardCRC();

        public int readSerialNumber(byte[] bPointer, int readCount)
        {
            int wLength = 0;

            bPointer[0] = 0x46;     // TesTORK as Target
            bPointer[1] = 0xA2;     // Send "read EEPROM", command.
            bPointer[2] = 0x00;     // sequence number (can set to zero)
            bPointer[3] = 0x01;     // payload size (which bank to read)
            bPointer[4] = 0x00;     // wireless sequence (low byte) (can set to zero)
            bPointer[5] = 0x00;     // wireless sequence (high byte) (can set to zero)

            switch (readCount)
            {
                case 2:
                    bPointer[6] = 0x09;     // first bank of Serial number
                    bPointer[7] = 0x90;     // new CRC
                    wLength = 8;
                    break;
                case 1:
                    bPointer[6] = 0x0A;     // second bank of Serial number
                    bPointer[7] = 0x91;     // new CRC
                    wLength = 8;
                    break;
                default:
                    break;
            }

            return wLength;
        }

        public int writeTesTORKserialNumber(byte[] bPointer, int writeCount, string serialNumber)
        {
            int wLength = 0;
            int commandPoint = 0;
            string addNull = null;

            serialNumber += addNull;

            bPointer[0] = 0x46;     // TesTORK as Target
            bPointer[1] = 0xA0;     // command to write to EEPROM
            bPointer[2] = 0x00;     // sequence number (can set to zero)
            bPointer[3] = 0x12;     // payload size
            bPointer[4] = 0x00;     // wireless sequence (low byte) (can set to zero)
            bPointer[5] = 0x00;     // wireless sequence (high byte) (can set to zero)
            bPointer[7] = 0x00;     // Status byte always set to zero

            byte[] toSend = new byte[32];
            byte[] serialNumberArray = Encoding.ASCII.GetBytes(serialNumber); // Convert string to byte array
            Buffer.BlockCopy(serialNumberArray, 0, toSend, 0, serialNumberArray.Length);
            toSend[serialNumberArray.Length] = 0x00;
            for(int count = serialNumberArray.Length+1; count < 32; count++)
            {
                toSend[count] = 0xFF;
            }

             switch (writeCount)
            {
                case 2:
                    bPointer[6] = 0x09;                     // write to first page of serial number location
                    commandPoint = 8;                       // first location to load serial number
                    for (int count = 0; count < 16; count++)
                    {
                        bPointer[commandPoint] = toSend[count];
                        commandPoint++;
                    }
                    bPointer[24] = canrigCRC.CalculateCRC(bPointer, 24);
                    wLength = 25;
                    break;
                case 1:
                    bPointer[6] = 0x0A;                     // write to second page of serial number location
                    commandPoint = 8;                       // first location to load serial number
                    for (int count = 16; count < 32; count++)
                    {
                        bPointer[commandPoint] = toSend[count];
                        commandPoint++;
                    }
                    bPointer[24] = canrigCRC.CalculateCRC(bPointer, 24);
                    wLength = 25;
                    break;
            }

            return wLength;
        }

        public void test(byte[] bPointer, ref string pString)
        {
            bPointer[0] = 51;
            bPointer[1] = 52;
            bPointer[2] = 53;

            pString = "Changed";
        }

        public int sendNoCommand(byte[] bPointer)
        {
            bPointer[0] = 0x46;     // TesTORK as Target
            bPointer[1] = 0x80;     // No Command, command
            bPointer[2] = 0x00;     // sequence number (can set to zero)
            bPointer[3] = 0x00;     // payload size
            bPointer[4] = 0x00;     // wireless sequence (low byte) (can set to zero)
            bPointer[5] = 0x00;     // wireless sequence (high byte) (can set to zero)
            bPointer[6] = 0xE6;     // CRC
            return 7;

        }
    }
}
