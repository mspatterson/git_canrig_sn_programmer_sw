﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canrig_SN_Programmer
{
    class WirelessDevice
    {
        CanrigStandardCRC canrigCRC = new CanrigStandardCRC();

        private byte getTarget(string target)
        {
            switch (target)
            {
                case "TesTORK":
                    return 0x46;
                case "MPLS":
                    return 0x81;
                case "Link Tilt Ind.":
                    return 0x46;     // Needs to change cause TesTORK and LTI should not be the same!
                default:
                    MessageBox.Show("Invalid device");
                    return 0;
            }
        }

        public int readWirelessDeviceSerialNumber(byte[] bPointer, int readCount, string target)
        {
            int wLength = 0;

            bPointer[0] = getTarget(target);
            bPointer[1] = 0xA2;             // Send "read EEPROM", command.
            bPointer[2] = 0x00;             // sequence number (can set to zero)
            bPointer[3] = 0x01;             // payload size (which bank to read)
            bPointer[4] = 0x00;             // wireless sequence (low byte) (can set to zero)
            bPointer[5] = 0x00;             // wireless sequence (high byte) (can set to zero)

            switch (readCount)
            {
                case 2:
                    bPointer[6] = 0x09;     // first bank of Serial number
                    //bPointer[7] = 0x90;
                    //wLength = 8;
                    break;
                case 1:
                    bPointer[6] = 0x0A;     // second bank of Serial number
                    //bPointer[7] = 0x91;     // new CRC
                    //wLength = 8;
                    break;
                default:
                    break;
            }

            bPointer[7] = canrigCRC.CalculateCRC(bPointer, 7);      // new CRC
            wLength = 8;

            return wLength;
        }

        public int writeWirelessDeviceSerialNumber(byte[] bPointer, int writeCount, string serialNumber, string target)
        {
            int wLength = 0;
            int commandPoint = 0;
            string addNull = null;

            serialNumber += addNull;

            bPointer[0] = getTarget(target);
            bPointer[1] = 0xA0;     // command to write to EEPROM
            bPointer[2] = 0x00;     // sequence number (can set to zero)
            bPointer[3] = 0x12;     // payload size
            bPointer[4] = 0x00;     // wireless sequence (low byte) (can set to zero)
            bPointer[5] = 0x00;     // wireless sequence (high byte) (can set to zero)
            bPointer[7] = 0x00;     // Status byte always set to zero

            byte[] toSend = new byte[32];
            byte[] serialNumberArray = Encoding.ASCII.GetBytes(serialNumber); // Convert string to byte array
            Buffer.BlockCopy(serialNumberArray, 0, toSend, 0, serialNumberArray.Length);
            toSend[serialNumberArray.Length] = 0x00;
            for (int count = serialNumberArray.Length + 1; count < 32; count++)
            {
                toSend[count] = 0xFF;
            }

            switch (writeCount)
            {
                case 2:
                    bPointer[6] = 0x09;                     // write to first page of serial number location
                    commandPoint = 8;                       // first location to load serial number
                    for (int count = 0; count < 16; count++)
                    {
                        bPointer[commandPoint] = toSend[count];
                        commandPoint++;
                    }
                    bPointer[24] = canrigCRC.CalculateCRC(bPointer, 24);
                    wLength = 25;
                    break;
                case 1:
                    bPointer[6] = 0x0A;                     // write to second page of serial number location
                    commandPoint = 8;                       // first location to load serial number
                    for (int count = 16; count < 32; count++)
                    {
                        bPointer[commandPoint] = toSend[count];
                        commandPoint++;
                    }
                    bPointer[24] = canrigCRC.CalculateCRC(bPointer, 24);
                    wLength = 25;
                    break;
            }

            return wLength;
        }

        public int sendNoCommand(byte[] bPointer, string target)
        {
            bPointer[0] = getTarget(target);
            bPointer[1] = 0x80;     // No Command, command
            bPointer[2] = 0x00;     // sequence number (can set to zero)
            bPointer[3] = 0x00;     // payload size
            bPointer[4] = 0x00;     // wireless sequence (low byte) (can set to zero)
            bPointer[5] = 0x00;     // wireless sequence (high byte) (can set to zero)
            bPointer[6] = canrigCRC.CalculateCRC(bPointer, 6);
            return 7;

        }
    }
}
