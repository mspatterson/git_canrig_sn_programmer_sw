﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canrig_SN_Programmer
{
    class CanrigStandardCRC
    {
        public byte CalculateCRC(byte[] bPointer, int wLength)
        {
            if (wLength > 100) return 0;
            const int c1 = 52845;
            const int c2 = 22719;
            int r;
            byte i;
            long sum;
            byte cipher;

            r = 55665;
            sum = 0;

            for (i = 0; i < wLength; i++)
            {
                cipher = (byte)(bPointer[i] ^ (r >> 8));
                r = (cipher + r) * c1 + c2;
                sum += cipher;
            }

            return (byte)sum;
        }
    }



}
