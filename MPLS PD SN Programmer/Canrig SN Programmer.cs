﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;


namespace Canrig_SN_Programmer
{
    public partial class frmCanrigSNprogrammer : Form
    {
        string dataIN;
        int BRcommLength;                               // Used to store length of Base Radio data from serial port
        int readButtonPressed = 0;                      // flag to trigger read when next RFC comes in 
        int writeButtonPressed = 0;                     // flag to trigger write when next RFC comes in
        byte[] BaseRadioBuf = new byte[920];
        int targetCommLength;
        byte[] targetBuf = new byte[500];
        enum DeviceType {
            TesTORK,
            MPLS,
            PlugDetecor,
            LTI
        };
        byte deviceMode = (byte)DeviceType.TesTORK;
        System.Windows.Forms.Timer tTarget = new System.Windows.Forms.Timer();      // used to determine if target device has gone missing
        System.Windows.Forms.Timer tBaseRadio = new System.Windows.Forms.Timer();   // used to determine if Base Radio has gone missing
        System.Windows.Forms.Timer tProgReadTimeout = new System.Windows.Forms.Timer(); //will timeout if program or read takes too long.
        System.Threading.Thread CloseDownBaseRadio;
        System.Threading.Thread CloseDownTarget;

        const byte overhead = 7;

        CanrigStandardCRC canrigCRC= new CanrigStandardCRC();   // Class for checking / creating a CRC
       
        WirelessDevice wirelessDevice = new WirelessDevice();   // Class for Wireless Communications

        public frmCanrigSNprogrammer()
        {
            InitializeComponent();
        }

        private void txtSerialNumberIn_TextChanged(object sender, EventArgs e)
        {
            int SNdataLength = txtBoxSerialNumberOut.TextLength;

            lblNumberOfChar.Text = string.Format("{0:00}", SNdataLength);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cBoxRadio.Items.Add("Link 1");
            cBoxRadio.Items.Add("Link 2");
            cBoxRadio.SelectedItem = "Link 1";                  // Set for TesTORK Initially
            cBoxTarget.Items.Add("TesTORK");
            cBoxTarget.Items.Add("MPLS");
            cBoxTarget.Items.Add("Plug Detector");
            cBoxTarget.Items.Add("Link Tilt Ind.");
            cBoxTarget.SelectedItem = "TesTORK";
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.RealTime;
            tTarget.Interval = 5000;                            // If no communications for 5 seconds
            tTarget.Tick += new EventHandler(TargetTimeout);       // Where to go when event triggers
            tBaseRadio.Interval = 100;                         // If no communications for 1/10 second
            tBaseRadio.Tick += new EventHandler(BaseRadioTimeout);
            tProgReadTimeout.Interval = 10000;                  // Give the system 10 seconds to read/write a wireless device
            tProgReadTimeout.Tick += new EventHandler(ProgReadTimeout); // Go here if the timeout completes
        }

        void TargetTimeout(object sender, EventArgs e)
        {
            proBarTargetConnected.Value = 0;
            cBoxTarget.Enabled = true;
        }

        void BaseRadioTimeout(object sender, EventArgs e)
        {
            progBarBaseRadio.Value = 0;
        }

        void ProgReadTimeout(object sender, EventArgs e)
        {
            btnProgram.Enabled = true;
            btnRead.Enabled = true;
            Application.UseWaitCursor = false;
            Cursor.Position = Cursor.Position;
            txtBoxSerialNumberIn.Text = "";
            tProgReadTimeout.Stop();
            MessageBox.Show("Operation Failed", "Read/Write");
        }

        private void btnBRportOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPortBaseRadio.PortName = cBoxBaseRadioComPort.Text;
                serialPortBaseRadio.BaudRate = 115200;
                serialPortBaseRadio.DataBits = 8;
                serialPortBaseRadio.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "One");
                serialPortBaseRadio.Parity = (Parity)Enum.Parse(typeof(Parity), "None");
                serialPortBaseRadio.DtrEnable = false;
                serialPortBaseRadio.RtsEnable = false;
                serialPortBaseRadio.ReadTimeout = 500;

                serialPortBaseRadio.Open();
                progBaseRadio.Value = 100;
                btnBRportOpen.Enabled = false;
                btnBRportClose.Enabled = true;
                lblStatusCom.Text = "ON";
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CloseSerialBaseRadioOnExit()
        {
            try
            {
                serialPortBaseRadio.DtrEnable = false;
                serialPortBaseRadio.RtsEnable = false;
                serialPortBaseRadio.DiscardInBuffer();
                serialPortBaseRadio.DiscardOutBuffer();
                serialPortBaseRadio.Dispose();
                serialPortBaseRadio.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Base Radio Serial Port");

            }
        }

        private void CloseSerialTargetOnExit()
        {
            try
            {
                serialPortTarget.DtrEnable = false;
                serialPortTarget.RtsEnable = false;
                serialPortTarget.DiscardInBuffer();
                serialPortTarget.DiscardOutBuffer();
                serialPortTarget.Dispose();
                serialPortTarget.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Target Serial Port");
            }
        }

        private void btnBRportClose_Click(object sender, EventArgs e)
        {
            if (serialPortBaseRadio.IsOpen)
            {
                CloseDownBaseRadio = new System.Threading.Thread(new System.Threading.ThreadStart(CloseSerialBaseRadioOnExit));
                CloseDownBaseRadio.Start();
                progBaseRadio.Value = 0;
                btnBRportClose.Enabled = false;
                btnBRportOpen.Enabled = true;
                lblStatusCom.Text = "OFF";
                progBarBaseRadio.Value = 0;
            }
        }

        private void btnTargetPortOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPortTarget.PortName = cBoxTargetPort.Text;
                if (cBoxTarget.Text == "Plug Detector") serialPortTarget.BaudRate = 19200;
                else serialPortTarget.BaudRate = 115200;
                serialPortTarget.DataBits = 8;
                serialPortTarget.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "One");
                serialPortTarget.Parity = (Parity)Enum.Parse(typeof(Parity), "None");
                serialPortTarget.DtrEnable = false;
                serialPortTarget.RtsEnable = false;
                serialPortTarget.ReadTimeout = 500;
                serialPortTarget.DataReceived += new SerialDataReceivedEventHandler(TargetDataReceivedHandler);

                serialPortTarget.Open();
                proBarTarget.Value = 100;
                btnTargetPortOpen.Enabled = false;
                cBoxTarget.Enabled = false;
                btnTargetPortClose.Enabled = true;
                lblMplsPortStatus.Text = "ON";
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnTargetPortClose_Click(object sender, EventArgs e)
        {
            if (serialPortTarget.IsOpen)
            {
                CloseDownTarget = new System.Threading.Thread(new System.Threading.ThreadStart(CloseSerialTargetOnExit));
                CloseDownTarget.Start();
                proBarTarget.Value = 0;
                btnTargetPortClose.Enabled = false;
                btnTargetPortOpen.Enabled = true;
                lblMplsPortStatus.Text = "OFF";
                proBarTargetConnected.Value = 0;
                cBoxTarget.Enabled = true;
            }

        }

        private void btnProgram_Click(object sender, EventArgs e)
        {
            
            if (txtBoxSerialNumberOut.TextLength < 21)
            {
                MessageBox.Show("Serial Number is too short!", "Entry Error");
                return;
            }
            if (serialPortTarget.IsOpen)
            {
                if(cBoxTarget.Text == "Plug Detector")
                {
                    SendSNtoPlugDetector();
                }
                else if (writeButtonPressed == 0 && proBarTargetConnected.Value == 100)
                {
                    writeButtonPressed = 2;
                    btnRead.Enabled = false;
                    Application.UseWaitCursor = true;
                    
                    tProgReadTimeout.Start();                               // if times out, resets everything.
                }
                else
                {
                    MessageBox.Show("Target not connected!");
                }
            }
            else
            {
                MessageBox.Show("Serial Port Not Open!", "Setup Error");
            }
        }

        private void SendSNtoPlugDetector()
        {
            byte[] boardCommand = { 0x40, 0x5E, 0x3F };         // 40=board address, 5E=Write SN, 3F=? (spacer)
            byte[] SNtext = Encoding.ASCII.GetBytes(txtBoxSerialNumberOut.Text); // convert string to byte array
 
            boardCommand[2] = (byte)SNtext.Count();             // replace 3F with length of SN data
            boardCommand[2]++;                                  // add one for the Null term.
            byte[] boardCommAndSN = new byte[SNtext.Count() + boardCommand.Count() + 2]; // create buffer to hold everything
            Buffer.BlockCopy(boardCommand, 0, boardCommAndSN, 0, boardCommand.Count()); // copy command into full buffer
            Buffer.BlockCopy(SNtext, 0, boardCommAndSN, boardCommand.Count(), SNtext.Length); // copy serial number into buffer

            boardCommAndSN[boardCommAndSN.Count() - 2] = 0x00;      // add in Null terminator
            //byte crcByte = CalculateCRC(boardCommand, boardCommand.Length);
            boardCommAndSN[boardCommAndSN.Count() - 1] = canrigCRC.CalculateCRC(boardCommAndSN, boardCommAndSN.Length - 1); //Load crcByte;

            serialPortTarget.DiscardInBuffer();
            serialPortTarget.Write(boardCommAndSN, 0, boardCommAndSN.Length);
        }

        private void txtSerialNumberOut_TextChanged(object sender, EventArgs e)
        {
            int SNdataLength = txtBoxSerialNumberIn.TextLength;

            lblNumOfCharOut.Text = string.Format("{0:00}", SNdataLength);       // Convert INT to String
        }

        private void txtBoxSerialNumberIn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 8 || (e.KeyChar >= 48 && e.KeyChar <= 57) ||
                e.KeyChar == 45 || (e.KeyChar >= 65 && e.KeyChar <= 90))  // Backspace, dash, number or upper case is OK
            {
                return;
            }
            if (e.KeyChar == 13)                            // Enter key
            {
                btnProgram.PerformClick();
                return;
            }
            if (e.KeyChar >= 97 && e.KeyChar <= 122)        // Lower case letter converted to upper
            {
                e.KeyChar = Char.ToUpper(e.KeyChar);
                return;
            }

            e.Handled = true;                               // all other standard ASCII is surpressed
        }

        private void ShowBaseRadioData(object sender, EventArgs e)
        {          
            if (BaseRadioBuf[0] != 0x99) return;                // if not from Base Radio
            if (BaseRadioBuf[1] != 0xA0) return;                // if not status update
            int payloadSize = BaseRadioBuf[3];
            
            var temp = new byte[payloadSize + overhead];          // put in new array to trim to lenght of reading
            Array.Copy(BaseRadioBuf, temp, payloadSize + overhead);
            //txtBoxSerialNumberIn.Text = string.Format ("{0:X}", CalculateCRC(BaseRadioBuf, packetLength + 6));
            if (canrigCRC.CalculateCRC(BaseRadioBuf, payloadSize + 6) != BaseRadioBuf[payloadSize + 6]) return;
            if(serialPortBaseRadio.IsOpen)
                progBarBaseRadio.Value = 100;
            tBaseRadio.Stop();                              // reset watchdog timer to keep progress bar 100%
            tBaseRadio.Start();
            lblLink1Channel.Text = "Ch " + string.Format("{0:00}", BaseRadioBuf[6]);
            lblLink2Channel.Text = "Ch " + string.Format("{0:00}", BaseRadioBuf[14]);
        }

        private void BaseRadioDataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
           if (serialPortBaseRadio.IsOpen)
            {
                if ((BRcommLength = serialPortBaseRadio.BytesToRead) < 0x24) return;      // Wait for full packet to come in
                if(BRcommLength > 100) BRcommLength = 100;                              // Somethings gone wrong
                serialPortBaseRadio.Read(BaseRadioBuf, 0, BRcommLength);
                serialPortBaseRadio.DiscardInBuffer();                      // Insure buffer is cleared
                Invoke(new EventHandler(ShowBaseRadioData));                         // serial port is a different thread. Need to call ShowData
                
            }
        }

        private void TargetDataReceivedHandler(object sender, SerialDataReceivedEventArgs e) // Target Serial Data In
        {
            int minimumBytes = 0;
            if (serialPortTarget.IsOpen)
            {
                switch (deviceMode)                 // Checking data IN size per mode, cause windows does not wait till data stopped
                {
                    case (byte)DeviceType.TesTORK:
                        if (readButtonPressed != 0 || writeButtonPressed != 0)
                            minimumBytes = 45;
                        else
                            minimumBytes = 59;
                        break;
                    case (byte)DeviceType.MPLS:
                        if (readButtonPressed != 0 || writeButtonPressed != 0)
                            minimumBytes = 45;
                        else
                            minimumBytes = 95;
                        break;
                    case (byte)DeviceType.PlugDetecor:
                        if ((targetCommLength = serialPortTarget.BytesToRead) < 40) return;
                        break;
                    case (byte)DeviceType.LTI:
                        if (readButtonPressed != 0 || writeButtonPressed != 0)
                            minimumBytes = 45;
                        else
                            minimumBytes = 59;
                        break;
                }
                if ((targetCommLength = serialPortTarget.BytesToRead) < minimumBytes) return;
                if (targetCommLength > 100) targetCommLength = 100;
                serialPortTarget.Read(targetBuf, 0, targetCommLength);
                serialPortTarget.DiscardInBuffer();

                Invoke(new EventHandler(ShowTargetData));
            }
        }

        private void findStartOfPacket()
        {
            int i = 0;
            byte findHead = 0x29;

            switch (cBoxTarget.Text)
            {
                case "Plug Detector":
                    findHead = 0x45;
                    break;
                case "TesTORK":
                    findHead = 0x29;
                    break;
                case "MPLS":
                    findHead = 0x15;
                    break;
                case "Link Tilt Ind.":
                    findHead = 0x29;
                    break;
            }

            for (i = 0; i < 100; i++)
            {
                if (targetBuf[i] == findHead)
                    break;
            }

            Array.Copy(targetBuf, i, targetBuf, 0, targetBuf.Length - i);
        }
        private void ShowTargetData(object sender, EventArgs e)
        {
            var boardCommand = new byte[100];
            int commandLength = 0;

            findStartOfPacket();

            txtTargetData.Text = BitConverter.ToString(targetBuf);   // raw data from port. used for debugging. Will probably remove it

            if (cBoxTarget.Text == "Plug Detector")
            {
                if (targetBuf[0] != 0x45) return;
                int findNull = 0;
                dataIN = getPayload(); // dataIN.Remove(0, 11);
                int count = dataIN.Length;
                while (dataIN[findNull] != 0)
                {
                    findNull++;
                    if (findNull > dataIN.Length)
                    {
                        MessageBox.Show("Invalid Packet!", "Comms Error");
                        return;
                    }
                }
                dataIN = dataIN.Remove(findNull, dataIN.Length - findNull);
                txtBoxSerialNumberIn.Text = dataIN;
            }
            else              // if a wireless device
            {
                if (targetBuf[0] != 0x29 && targetBuf[0] != 0x15) return;           // Not start of buffer. Throw out message and wait till next RFC

                if (targetBuf[1] == 0x2A || targetBuf[1] == 0x35)                   // if message in is response to EEPROM read (also received after a write)
                {
                    string SNread = "";
                    SNread = getPayload();
                    readWriteButtonHandler(SNread);
                }

                if (targetBuf[1] == 0x26 || targetBuf[1] == 0x31)                    // incoming packet is an RFC?
                {
                    if(readButtonPressed == 0 && writeButtonPressed == 0) commandLength = wirelessDevice.sendNoCommand(boardCommand, cBoxTarget.Text);
                
                    if (readButtonPressed != 0)                 // if incoming packet is RFC and there is a pending read serial number request
                    {
                        commandLength = wirelessDevice.readWirelessDeviceSerialNumber(boardCommand, readButtonPressed, cBoxTarget.Text);
                    }
                    if (writeButtonPressed != 0)
                    {
                        commandLength = wirelessDevice.writeWirelessDeviceSerialNumber(boardCommand, writeButtonPressed, txtBoxSerialNumberOut.Text, cBoxTarget.Text);
                    }
                }

                if(serialPortTarget.IsOpen)
                    proBarTargetConnected.Value = 100;                                  // if was not connected before
                tTarget.Stop();                                                         // restart timeout timer
                tTarget.Start();                                                        // automatically starts from set interval
            }
            serialPortTarget.Write(boardCommand, 0, commandLength);

            var temp = new byte[targetBuf[3] + 7];              // create array the size of the packet. Serial port reads more than packet size
            Array.Copy(targetBuf, temp, targetBuf[3] + 7);
            Array.Clear(targetBuf, 0, targetBuf.Length);
        }

        private string getPayload()
        {
            int overHead = 7;               // values for anything other than Plug Detector
            int headerSize = 8;
            int packetSizeLocation = 3;

            if (cBoxTarget.Text == "Plug Detector")
            {
                overHead = 12;               // header and CRC for Plug Detector packet
                headerSize = 11;
                packetSizeLocation = 2;
            }
            var temp = new byte[targetBuf[packetSizeLocation] + overHead];  // create array the size of the packet. Serial port reads more than packet size
            Array.Copy(targetBuf, temp, targetBuf[packetSizeLocation] + overHead);
            string payLoad = System.Text.Encoding.UTF8.GetString(temp);     // convert byte array to string
            payLoad = payLoad.Remove(0, headerSize);                        // remove header 
            payLoad = payLoad.Remove(payLoad.Length - 1, 1);                // remove CRC an leave only payload
            return payLoad;
        }

        private void readWriteButtonHandler(string payLoad)
        {

            if (targetBuf[6] == 0x09)                       // read first bank of serial number
            {
                if (readButtonPressed == 2 || writeButtonPressed == 2)
                {
                    txtBoxSerialNumberIn.Text = payLoad;        // clear the box and show first half of serial number
                }
                
                if (readButtonPressed != 0) readButtonPressed = 1;  // decrement which ever button was pressed
                if (writeButtonPressed != 0) writeButtonPressed = 1;
            }
            if (targetBuf[6] == 0x0A)                       // read second bank of serial number
            {
                if (readButtonPressed == 1 || writeButtonPressed == 1)
                {
                    txtBoxSerialNumberIn.Text += payLoad;       // a serial number must be greater than 21 characters.
                }
                if (readButtonPressed != 0)                 // if was doing a read
                {
                    readButtonPressed = 0;
                    btnProgram.Enabled = true;
                   
                }
                if (writeButtonPressed != 0)                // if was programming
                {
                    writeButtonPressed = 0;
                    btnRead.Enabled = true;
                }
                tProgReadTimeout.Stop();                    // Disable timeout
                Application.UseWaitCursor = false;          // return cursor
                Cursor.Position = Cursor.Position;
            }
            
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            if (cBoxTarget.Text == "Plug Detector" && serialPortTarget.IsOpen)
            {
                serialPortTarget.DiscardInBuffer();
                byte[] boardCommand = { 0x40, 0x5C, 0x00, 0x54 }; // 0x40 = board, 0x5C = command, 0x00 = payload, 0x54 = CRC
                serialPortTarget.Write(boardCommand, 0, boardCommand.Length);
            }
            else if (proBarTarget.Value == 100)
            {
                readButtonPressed = 2;
                btnProgram.Enabled = false;                                     // Don't do a write during a read
                txtBoxSerialNumberIn.Text = "Reading... Please Wait";
                Application.UseWaitCursor = true;
                tProgReadTimeout.Start();
            }
            else MessageBox.Show("Target Not Connected!", "Setup Error");
        }

        private void btnChangeRadio1Channel_Click(object sender, EventArgs e)
        {
            if(progBarBaseRadio.Value != 100)
            {
                MessageBox.Show("Base Radio is not connected!", "Setup Error");
                return;
            }
            byte[] boardCommand = { 0x04, 0x12, 0x00, 0x02, 0x00, 0x00, 0x01, 0x10, 0x2A };// 04=board address, 12h=change chan, 01=radio(TNC), 10h=ch16
            if (cBoxRadio.Text == "Link 1") boardCommand[6] = 0x00;
            switch (cBoxChannel.Text)
            {
                case "":
                    MessageBox.Show("Please enter a value for channel", "Entry Error");
                    return;
                case "Channel 11":
                    boardCommand[7] = 0x0B;
                    break;
                case "Channel 12":
                    boardCommand[7] = 0x0C;
                    break;
                case "Channel 15":
                    boardCommand[7] = 0x0F;
                    break;
                case "Channel 16":
                    boardCommand[7] = 0x10;
                    break;
                case "Channel 20":
                    boardCommand[7] = 0x14;
                    break;
                case "Channel 21":
                    boardCommand[7] = 0x15;
                    break;
                case "Channel 25":
                    boardCommand[7] = 0x19;
                    break;
                default:
                    boardCommand[7] = 0x18;
                    break;
            }
            boardCommand[8] = canrigCRC.CalculateCRC(boardCommand, boardCommand.Length - 1);
            serialPortBaseRadio.DiscardInBuffer();
            serialPortBaseRadio.DiscardOutBuffer();
            serialPortBaseRadio.Write(boardCommand, 0, boardCommand.Length);
            tBaseRadio.Stop();                                   // One way to reset the timer
            tBaseRadio.Start();
            progBarBaseRadio.Value = 0;
        }

        private void cBoxComPort_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cBoxChannel_DropDown(object sender, EventArgs e)
        {
            cBoxChannel.Items.Clear();
            if (cBoxRadio.Text == "Link 1")
            {
                cBoxChannel.Items.Add("Channel 11");
                cBoxChannel.Items.Add("Channel 15");
                cBoxChannel.Items.Add("Channel 20");
                cBoxChannel.Items.Add("Channel 24");
            }
            else
            {
                cBoxChannel.Items.Add("Channel 12");
                cBoxChannel.Items.Add("Channel 16");
                cBoxChannel.Items.Add("Channel 21");
                cBoxChannel.Items.Add("Channel 24");
            }
            
        }

        private void cBoxTarget_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cBoxTarget.Text == "Plug Detector")     // the only wired device and is treated differently
            {
                deviceMode = (byte)DeviceType.PlugDetecor;
                if(serialPortBaseRadio.IsOpen)
                {
                    btnBRportClose_Click(sender, e); 
                }
                cBoxBaseRadioComPort.Enabled = false;
                btnBRportOpen.Enabled = false;
                progBarBaseRadio.Value = 0;
                progBaseRadio.Enabled = false;
                btnBRportClose.Enabled = false;
                cBoxRadio.Enabled = false;
                cBoxChannel.Enabled = false;
                btnChangeRadio1Channel.Enabled = false;
            }
            else
            {
                progBaseRadio.Enabled = true;
                if (serialPortBaseRadio.IsOpen)
                {
                    btnBRportClose.Enabled = true;
                    btnBRportOpen.Enabled = false;
                }
                else
                {
                    btnBRportClose.Enabled = false;
                    btnBRportOpen.Enabled = true;
                }
                cBoxBaseRadioComPort.Enabled = true;
                cBoxRadio.Enabled = true;
                cBoxChannel.Enabled = true;
                btnChangeRadio1Channel.Enabled = true;
                switch (cBoxTarget.Text)
                {
                    case "TesTORK":
                        cBoxRadio.Text = "Link 1";
                        deviceMode = (byte)DeviceType.TesTORK;
                        break;
                    case "MPLS":
                        cBoxRadio.Text = "Link 2";
                        deviceMode = (byte)DeviceType.MPLS;
                        break;
                    case "Link Tilt Ind.":
                        cBoxRadio.Text = "Link 1";
                        deviceMode = (byte)DeviceType.LTI;
                        break;
                    default:
                        break;
                }
            }
        }

        private void btnHowToUse_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1. Ensure Target device is powered up and connected to PC" +
                "\n\ta. Base Radio for TesTORK, MPLS and Link Tilt Ind." +
                "\n\tb. Programming cable for Plug Detector." +
                "\n2. Select Target Device from drop down box." +
                "\n3. Select Com Port for the Base Radio Communication from the drop down." +
                "\n\ta. Base Radio Generates 4 consecutive COM Port" +
                "\n\t\ti. The first COM Port is used for Communication" +
                "\n4. Click the ‘Open’ button for the Base Radio Communication." +
                "\n     Indicator below button should turn Green." +
                "\n     Communication Status for Base Radio should turn green." +
                "\n5. If programming a MPLS, Radio Link 2 Channel needs to be changed." +
                "\n\ta. Use Channel drop down box and set to 12." +
                "\n\tb. Click ‘Set Channel’ button" +
                "\n6. Select proper Com Port for the Target Communication from the drop down." +
                "\n\ta. Base Radio Generates 4 consecutive COM Ports" +
                "\n\t\ti. TesTORK and Link Tilt Ind uses the second COM Port" +
                "\n\t\tii. MPLS uses the third COM Port" +
                "\n\tb. Plug Detector Cable generates 1 COM Port" +
                "\n7. Click the ‘Open’ button for the Target Communication." +
                "\n     Indicator below button should turn Green." +
                "\n\ta. If connecting to a wireless device, wait for the Communication" +
                "\n\t     Status 'Wireless Target' to turn green." +
                "\n\tNote: Plug Detector has not status indicator." +
                "\n8. Enter Serial number in ‘Serial Number to Send’ box & click 'Program'." +
                "\n9. Once done it will read the programmed serial number back for verification.", "How To Use");
        }

        private void cBoxBaseRadioComPort_DropDown(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            cBoxBaseRadioComPort.Items.Clear();
            cBoxBaseRadioComPort.Items.AddRange(ports);
        }

        private void cBoxTargetPort_DropDown(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            cBoxTargetPort.Items.Clear();
            cBoxTargetPort.Items.AddRange(ports);
        }

        private void cBoxRadio_SelectedIndexChanged(object sender, EventArgs e)
        {
            cBoxChannel.Items.Clear();      // if radio focus changes, clear the channels
        }

        private void FrmCanrigSNprogrammer_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);                // Force closes this application. Needed cause of bug in windows serial comms
        }

        private void PicCanrigLogo_DoubleClick(object sender, EventArgs e)
        {

            this.Size = this.Size.Height == 322 ? new Size(651, 432) : new Size(651, 322);
        }
    }
}
