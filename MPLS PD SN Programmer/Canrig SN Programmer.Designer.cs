﻿namespace Canrig_SN_Programmer
{
    partial class frmCanrigSNprogrammer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCanrigSNprogrammer));
            this.serialPortBaseRadio = new System.IO.Ports.SerialPort(this.components);
            this.txtBoxSerialNumberOut = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNumberOfChar = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblStatusCom = new System.Windows.Forms.Label();
            this.btnBRportClose = new System.Windows.Forms.Button();
            this.progBaseRadio = new System.Windows.Forms.ProgressBar();
            this.btnBRportOpen = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cBoxBaseRadioComPort = new System.Windows.Forms.ComboBox();
            this.btnProgram = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnRead = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNumOfCharOut = new System.Windows.Forms.Label();
            this.txtBoxSerialNumberIn = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblMplsPortStatus = new System.Windows.Forms.Label();
            this.btnTargetPortClose = new System.Windows.Forms.Button();
            this.proBarTarget = new System.Windows.Forms.ProgressBar();
            this.btnTargetPortOpen = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cBoxTargetPort = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.proBarTargetConnected = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.progBarBaseRadio = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChangeRadio1Channel = new System.Windows.Forms.Button();
            this.serialPortTarget = new System.IO.Ports.SerialPort(this.components);
            this.txtTargetData = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblLink2Channel = new System.Windows.Forms.Label();
            this.lblLink1Channel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cBoxRadio = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cBoxChannel = new System.Windows.Forms.ComboBox();
            this.cBoxTarget = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.picCanrigLogo = new System.Windows.Forms.PictureBox();
            this.btnHowToUse = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCanrigLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // serialPortBaseRadio
            // 
            this.serialPortBaseRadio.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.BaseRadioDataReceivedHandler);
            // 
            // txtBoxSerialNumberOut
            // 
            this.txtBoxSerialNumberOut.AcceptsReturn = true;
            this.txtBoxSerialNumberOut.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxSerialNumberOut.Location = new System.Drawing.Point(9, 19);
            this.txtBoxSerialNumberOut.MaxLength = 31;
            this.txtBoxSerialNumberOut.Name = "txtBoxSerialNumberOut";
            this.txtBoxSerialNumberOut.Size = new System.Drawing.Size(343, 30);
            this.txtBoxSerialNumberOut.TabIndex = 1;
            this.txtBoxSerialNumberOut.WordWrap = false;
            this.txtBoxSerialNumberOut.TextChanged += new System.EventHandler(this.txtSerialNumberIn_TextChanged);
            this.txtBoxSerialNumberOut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxSerialNumberIn_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Number of Characters:";
            // 
            // lblNumberOfChar
            // 
            this.lblNumberOfChar.AutoSize = true;
            this.lblNumberOfChar.Location = new System.Drawing.Point(163, 55);
            this.lblNumberOfChar.Name = "lblNumberOfChar";
            this.lblNumberOfChar.Size = new System.Drawing.Size(24, 17);
            this.lblNumberOfChar.TabIndex = 3;
            this.lblNumberOfChar.Text = "00";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblStatusCom);
            this.groupBox1.Controls.Add(this.btnBRportClose);
            this.groupBox1.Controls.Add(this.progBaseRadio);
            this.groupBox1.Controls.Add(this.btnBRportOpen);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cBoxBaseRadioComPort);
            this.groupBox1.Location = new System.Drawing.Point(151, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(141, 232);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base Radio Communications";
            // 
            // lblStatusCom
            // 
            this.lblStatusCom.AutoSize = true;
            this.lblStatusCom.Location = new System.Drawing.Point(7, 168);
            this.lblStatusCom.Name = "lblStatusCom";
            this.lblStatusCom.Size = new System.Drawing.Size(27, 17);
            this.lblStatusCom.TabIndex = 5;
            this.lblStatusCom.Text = "Off";
            // 
            // btnBRportClose
            // 
            this.btnBRportClose.Enabled = false;
            this.btnBRportClose.Location = new System.Drawing.Point(10, 191);
            this.btnBRportClose.Name = "btnBRportClose";
            this.btnBRportClose.Size = new System.Drawing.Size(121, 35);
            this.btnBRportClose.TabIndex = 4;
            this.btnBRportClose.Text = "Close";
            this.btnBRportClose.UseVisualStyleBackColor = true;
            this.btnBRportClose.Click += new System.EventHandler(this.btnBRportClose_Click);
            // 
            // progBaseRadio
            // 
            this.progBaseRadio.Location = new System.Drawing.Point(10, 141);
            this.progBaseRadio.Name = "progBaseRadio";
            this.progBaseRadio.Size = new System.Drawing.Size(121, 23);
            this.progBaseRadio.TabIndex = 3;
            // 
            // btnBRportOpen
            // 
            this.btnBRportOpen.Location = new System.Drawing.Point(10, 98);
            this.btnBRportOpen.Name = "btnBRportOpen";
            this.btnBRportOpen.Size = new System.Drawing.Size(121, 36);
            this.btnBRportOpen.TabIndex = 2;
            this.btnBRportOpen.Text = "Open";
            this.btnBRportOpen.UseVisualStyleBackColor = true;
            this.btnBRportOpen.Click += new System.EventHandler(this.btnBRportOpen_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Com Port";
            // 
            // cBoxBaseRadioComPort
            // 
            this.cBoxBaseRadioComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxBaseRadioComPort.FormattingEnabled = true;
            this.cBoxBaseRadioComPort.Location = new System.Drawing.Point(10, 67);
            this.cBoxBaseRadioComPort.Name = "cBoxBaseRadioComPort";
            this.cBoxBaseRadioComPort.Size = new System.Drawing.Size(121, 24);
            this.cBoxBaseRadioComPort.TabIndex = 0;
            this.cBoxBaseRadioComPort.DropDown += new System.EventHandler(this.cBoxBaseRadioComPort_DropDown);
            this.cBoxBaseRadioComPort.SelectedIndexChanged += new System.EventHandler(this.cBoxComPort_SelectedIndexChanged);
            // 
            // btnProgram
            // 
            this.btnProgram.Location = new System.Drawing.Point(284, 55);
            this.btnProgram.Name = "btnProgram";
            this.btnProgram.Size = new System.Drawing.Size(75, 23);
            this.btnProgram.TabIndex = 5;
            this.btnProgram.Text = "Program";
            this.btnProgram.UseVisualStyleBackColor = true;
            this.btnProgram.Click += new System.EventHandler(this.btnProgram_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnProgram);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtBoxSerialNumberOut);
            this.groupBox2.Controls.Add(this.lblNumberOfChar);
            this.groupBox2.Location = new System.Drawing.Point(476, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(365, 85);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial Number to Send";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnRead);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.lblNumOfCharOut);
            this.groupBox3.Controls.Add(this.txtBoxSerialNumberIn);
            this.groupBox3.Location = new System.Drawing.Point(476, 123);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(365, 85);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Serial Number Read";
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(284, 55);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(75, 23);
            this.btnRead.TabIndex = 5;
            this.btnRead.Text = "Read";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Number of Characters:";
            // 
            // lblNumOfCharOut
            // 
            this.lblNumOfCharOut.AutoSize = true;
            this.lblNumOfCharOut.Location = new System.Drawing.Point(163, 55);
            this.lblNumOfCharOut.Name = "lblNumOfCharOut";
            this.lblNumOfCharOut.Size = new System.Drawing.Size(24, 17);
            this.lblNumOfCharOut.TabIndex = 3;
            this.lblNumOfCharOut.Text = "00";
            // 
            // txtBoxSerialNumberIn
            // 
            this.txtBoxSerialNumberIn.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxSerialNumberIn.Location = new System.Drawing.Point(9, 21);
            this.txtBoxSerialNumberIn.Multiline = true;
            this.txtBoxSerialNumberIn.Name = "txtBoxSerialNumberIn";
            this.txtBoxSerialNumberIn.Size = new System.Drawing.Size(343, 30);
            this.txtBoxSerialNumberIn.TabIndex = 1;
            this.txtBoxSerialNumberIn.TextChanged += new System.EventHandler(this.txtSerialNumberOut_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblMplsPortStatus);
            this.groupBox4.Controls.Add(this.btnTargetPortClose);
            this.groupBox4.Controls.Add(this.proBarTarget);
            this.groupBox4.Controls.Add(this.btnTargetPortOpen);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.cBoxTargetPort);
            this.groupBox4.Location = new System.Drawing.Point(298, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(141, 232);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Target Communications";
            // 
            // lblMplsPortStatus
            // 
            this.lblMplsPortStatus.AutoSize = true;
            this.lblMplsPortStatus.Location = new System.Drawing.Point(7, 168);
            this.lblMplsPortStatus.Name = "lblMplsPortStatus";
            this.lblMplsPortStatus.Size = new System.Drawing.Size(27, 17);
            this.lblMplsPortStatus.TabIndex = 5;
            this.lblMplsPortStatus.Text = "Off";
            // 
            // btnTargetPortClose
            // 
            this.btnTargetPortClose.Enabled = false;
            this.btnTargetPortClose.Location = new System.Drawing.Point(10, 191);
            this.btnTargetPortClose.Name = "btnTargetPortClose";
            this.btnTargetPortClose.Size = new System.Drawing.Size(121, 35);
            this.btnTargetPortClose.TabIndex = 4;
            this.btnTargetPortClose.Text = "Close";
            this.btnTargetPortClose.UseVisualStyleBackColor = true;
            this.btnTargetPortClose.Click += new System.EventHandler(this.btnTargetPortClose_Click);
            // 
            // proBarTarget
            // 
            this.proBarTarget.Location = new System.Drawing.Point(10, 141);
            this.proBarTarget.Name = "proBarTarget";
            this.proBarTarget.Size = new System.Drawing.Size(121, 23);
            this.proBarTarget.TabIndex = 3;
            // 
            // btnTargetPortOpen
            // 
            this.btnTargetPortOpen.Location = new System.Drawing.Point(10, 98);
            this.btnTargetPortOpen.Name = "btnTargetPortOpen";
            this.btnTargetPortOpen.Size = new System.Drawing.Size(121, 36);
            this.btnTargetPortOpen.TabIndex = 2;
            this.btnTargetPortOpen.Text = "Open";
            this.btnTargetPortOpen.UseVisualStyleBackColor = true;
            this.btnTargetPortOpen.Click += new System.EventHandler(this.btnTargetPortOpen_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Com Port";
            // 
            // cBoxTargetPort
            // 
            this.cBoxTargetPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxTargetPort.FormattingEnabled = true;
            this.cBoxTargetPort.Location = new System.Drawing.Point(10, 67);
            this.cBoxTargetPort.Name = "cBoxTargetPort";
            this.cBoxTargetPort.Size = new System.Drawing.Size(121, 24);
            this.cBoxTargetPort.TabIndex = 0;
            this.cBoxTargetPort.DropDown += new System.EventHandler(this.cBoxTargetPort_DropDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.proBarTargetConnected);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.progBarBaseRadio);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(4, 82);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(141, 162);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Communication Status";
            // 
            // proBarTargetConnected
            // 
            this.proBarTargetConnected.Location = new System.Drawing.Point(10, 121);
            this.proBarTargetConnected.Name = "proBarTargetConnected";
            this.proBarTargetConnected.Size = new System.Drawing.Size(121, 23);
            this.proBarTargetConnected.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Wireless Target";
            // 
            // progBarBaseRadio
            // 
            this.progBarBaseRadio.Location = new System.Drawing.Point(10, 72);
            this.progBarBaseRadio.Name = "progBarBaseRadio";
            this.progBarBaseRadio.Size = new System.Drawing.Size(121, 23);
            this.progBarBaseRadio.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Base Radio";
            // 
            // btnChangeRadio1Channel
            // 
            this.btnChangeRadio1Channel.Location = new System.Drawing.Point(208, 28);
            this.btnChangeRadio1Channel.Name = "btnChangeRadio1Channel";
            this.btnChangeRadio1Channel.Size = new System.Drawing.Size(80, 56);
            this.btnChangeRadio1Channel.TabIndex = 8;
            this.btnChangeRadio1Channel.Text = "Set Channel";
            this.btnChangeRadio1Channel.UseVisualStyleBackColor = true;
            this.btnChangeRadio1Channel.Click += new System.EventHandler(this.btnChangeRadio1Channel_Click);
            // 
            // serialPortTarget
            // 
            this.serialPortTarget.BaudRate = 115200;
            // 
            // txtTargetData
            // 
            this.txtTargetData.Font = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTargetData.Location = new System.Drawing.Point(4, 351);
            this.txtTargetData.Multiline = true;
            this.txtTargetData.Name = "txtTargetData";
            this.txtTargetData.ReadOnly = true;
            this.txtTargetData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTargetData.Size = new System.Drawing.Size(374, 129);
            this.txtTargetData.TabIndex = 9;
            this.txtTargetData.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblLink2Channel);
            this.groupBox6.Controls.Add(this.lblLink1Channel);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.cBoxRadio);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.cBoxChannel);
            this.groupBox6.Controls.Add(this.btnChangeRadio1Channel);
            this.groupBox6.Location = new System.Drawing.Point(4, 250);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(435, 95);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Set Radio Channel";
            // 
            // lblLink2Channel
            // 
            this.lblLink2Channel.AutoSize = true;
            this.lblLink2Channel.Location = new System.Drawing.Point(365, 65);
            this.lblLink2Channel.Name = "lblLink2Channel";
            this.lblLink2Channel.Size = new System.Drawing.Size(37, 17);
            this.lblLink2Channel.TabIndex = 18;
            this.lblLink2Channel.Text = "Ch ?";
            // 
            // lblLink1Channel
            // 
            this.lblLink1Channel.AutoSize = true;
            this.lblLink1Channel.Location = new System.Drawing.Point(304, 66);
            this.lblLink1Channel.Name = "lblLink1Channel";
            this.lblLink1Channel.Size = new System.Drawing.Size(37, 17);
            this.lblLink1Channel.TabIndex = 17;
            this.lblLink1Channel.Text = "Ch ?";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(365, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "Link 2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(301, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 17);
            this.label9.TabIndex = 15;
            this.label9.Text = "Link 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Radio";
            // 
            // cBoxRadio
            // 
            this.cBoxRadio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxRadio.FormattingEnabled = true;
            this.cBoxRadio.Location = new System.Drawing.Point(79, 28);
            this.cBoxRadio.Name = "cBoxRadio";
            this.cBoxRadio.Size = new System.Drawing.Size(121, 24);
            this.cBoxRadio.TabIndex = 13;
            this.cBoxRadio.SelectedIndexChanged += new System.EventHandler(this.cBoxRadio_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Channel";
            // 
            // cBoxChannel
            // 
            this.cBoxChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxChannel.FormattingEnabled = true;
            this.cBoxChannel.Location = new System.Drawing.Point(79, 58);
            this.cBoxChannel.Name = "cBoxChannel";
            this.cBoxChannel.Size = new System.Drawing.Size(121, 24);
            this.cBoxChannel.TabIndex = 10;
            this.cBoxChannel.DropDown += new System.EventHandler(this.cBoxChannel_DropDown);
            // 
            // cBoxTarget
            // 
            this.cBoxTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxTarget.FormattingEnabled = true;
            this.cBoxTarget.Location = new System.Drawing.Point(8, 21);
            this.cBoxTarget.Name = "cBoxTarget";
            this.cBoxTarget.Size = new System.Drawing.Size(121, 24);
            this.cBoxTarget.TabIndex = 9;
            this.cBoxTarget.SelectedIndexChanged += new System.EventHandler(this.cBoxTarget_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.cBoxTarget);
            this.groupBox7.Location = new System.Drawing.Point(4, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(141, 64);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Target Device";
            // 
            // picCanrigLogo
            // 
            this.picCanrigLogo.Image = global::Canrig_SN_Programmer.Properties.Resources.equipment_canrig_banner_0_031418_DV_0;
            this.picCanrigLogo.InitialImage = ((System.Drawing.Image)(resources.GetObject("picCanrigLogo.InitialImage")));
            this.picCanrigLogo.Location = new System.Drawing.Point(654, 215);
            this.picCanrigLogo.Name = "picCanrigLogo";
            this.picCanrigLogo.Size = new System.Drawing.Size(187, 130);
            this.picCanrigLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCanrigLogo.TabIndex = 11;
            this.picCanrigLogo.TabStop = false;
            this.picCanrigLogo.DoubleClick += new System.EventHandler(this.PicCanrigLogo_DoubleClick);
            // 
            // btnHowToUse
            // 
            this.btnHowToUse.Location = new System.Drawing.Point(481, 215);
            this.btnHowToUse.Name = "btnHowToUse";
            this.btnHowToUse.Size = new System.Drawing.Size(161, 129);
            this.btnHowToUse.TabIndex = 12;
            this.btnHowToUse.Text = "How To Use";
            this.btnHowToUse.UseVisualStyleBackColor = true;
            this.btnHowToUse.Click += new System.EventHandler(this.btnHowToUse_Click);
            // 
            // frmCanrigSNprogrammer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 348);
            this.Controls.Add(this.btnHowToUse);
            this.Controls.Add(this.picCanrigLogo);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.txtTargetData);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmCanrigSNprogrammer";
            this.Text = "Canrig Serial Number Programmer Ver 1.0.0.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCanrigSNprogrammer_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCanrigLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPortBaseRadio;
        private System.Windows.Forms.TextBox txtBoxSerialNumberOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNumberOfChar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cBoxBaseRadioComPort;
        private System.Windows.Forms.Button btnBRportOpen;
        private System.Windows.Forms.ProgressBar progBaseRadio;
        private System.Windows.Forms.Button btnBRportClose;
        private System.Windows.Forms.Label lblStatusCom;
        private System.Windows.Forms.Button btnProgram;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxSerialNumberIn;
        private System.Windows.Forms.Label lblNumOfCharOut;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblMplsPortStatus;
        private System.Windows.Forms.Button btnTargetPortClose;
        private System.Windows.Forms.ProgressBar proBarTarget;
        private System.Windows.Forms.Button btnTargetPortOpen;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cBoxTargetPort;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ProgressBar proBarTargetConnected;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar progBarBaseRadio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnChangeRadio1Channel;
        private System.IO.Ports.SerialPort serialPortTarget;
        private System.Windows.Forms.TextBox txtTargetData;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cBoxChannel;
        private System.Windows.Forms.ComboBox cBoxTarget;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cBoxRadio;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.PictureBox picCanrigLogo;
        private System.Windows.Forms.Button btnHowToUse;
        private System.Windows.Forms.Label lblLink2Channel;
        private System.Windows.Forms.Label lblLink1Channel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}

